FactoryGirl.define do
  factory :game do
    name 'Decepticolor'
    description 'This is the description of an awesome game'
  end
end
