class RemoveDeveloperInvolvement < ActiveRecord::Migration
  def up
    drop_table :developer_involvements
  end

  def down
    create_table :developer_involvements do |t|
      t.references :game
      t.references :developer

      t.timestamps
    end
  end
end
