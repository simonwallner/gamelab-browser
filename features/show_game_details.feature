Feature: Show game details on its 'game page'

	In order to inform myself about a game,
	As a user,
	I want to view details about it on its 'game page'
	
	Scenario: Show game details.
		Given There is a game called "Decpticolor"
		When I am on the game's page
		Then I should see the game's title
		And I should see the game's description
		
	Scenario: Show the developers of a game.
		Given a game called "Decepticolor"
		And a developer of that game called "Bender"
		And a developer of that game called "Zoidberg"
		When I am on the game's page
		Then I should see the developers' names