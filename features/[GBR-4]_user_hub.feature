Feature: User hub
	As a user,
	to have a central starting point,
	I want to have a main "hub page"

	Scenario: navigate to the hub page
		Given I am signed in
		When I visit "/hub"
		Then I should be on the hub page
	
	Scenario: Thou shalt be signed in to enter the castle (hub, that is)
		Given I am a visitor
		When I visit the hub page
		Then I should be on the sign in page
		And I should see "You need to sign in or sign up before continuing"
	
	Scenario: Provide link to account settings
		Given I am signed in
		When I am on the hub page
		Then then I should see a link to the account settings