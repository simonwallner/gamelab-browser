class CreateDeveloperInvolvements < ActiveRecord::Migration
  def change
    create_table :developer_involvements do |t|
      t.references :game
      t.references :developer

      t.timestamps
    end
  end
end
