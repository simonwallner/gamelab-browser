World(ShowMeTheCookies)

When /^I visit the GameLab website$/ do
  visit '/'
end

Then /^I should be greeted$/ do
  page.should have_content("hello")
end

Given /^(?:There is )?a game called "([^"]*)"$/ do |game_name|
  @game = create(:game, :name => game_name)
end

When /^I am on the game's page$/ do
  visit game_path(@game)
end

Then /^I should see the game's title$/ do
  page.should have_content(@game.name)
end

Then /^I should see the game's description$/ do
  page.should have_content(@game.description)
end

Given /^(?:there is )?a developer called "([^"]*)"$/ do |developer_name|
  @developer = create(:developer, :name => developer_name)
end

When /^I am on the developer's page$/ do
  visit developer_path(@developer)
end

Then /^I should see the developer's name$/ do
  page.should have_content(@developer.name)
end

Then /^the path should be "([^"]*)"$/ do |path|
  current_path = URI.parse(current_url).path
  current_path.should == path
end

Given /^a developer of that game called "([^"]*)"$/ do |developer_name|
  dev = create(:developer, :name => developer_name)
  DeveloperInvolvement.new(:developer => dev, :game => @game).save
end

Then /^I should see the developers' names$/ do
  @game.developers.each do |dev|
    page.should have_content(dev.name)
  end
end

Given /^I am an unregistered visitor$/ do
  @visitor = build(:user)
  User.find_by_email(@visitor.email).should be_nil
end

Given /^I am on the sign up page$/ do
  visit new_user_registration_path
end

When /^I enter my registration credentials$/ do
  fill_in "user_email", :with => @visitor.email
  fill_in "user_password", :with => @visitor.password
  fill_in "user_password_confirmation", :with => @visitor.password
  click_button "Sign up"
end

Then /^a user account should have been created for me$/ do
  @user = User.find_by_email(@visitor.email)
  @user.should_not be_nil
end

Then /^a confirmation email should have been sent to me$/ do
  email = ActionMailer::Base.deliveries.last
  email.from.should == ["noreply@example.com"]
  email.to.should == [@user.email]
  email.body.should include "confirm your account"
end

Then /^I should not be signed in$/ do
  visit '/'
  page.should have_content "sign in"
end

Given /^I register a user account$/ do
  visit new_user_registration_path
  fill_in "user_email", :with => @visitor.email
  fill_in "user_password", :with => @visitor.password
  fill_in "user_password_confirmation", :with => @visitor.password
  click_button "Sign up"
end

Given /^I follow the (?:confirmation |recovery )?link in the email$/ do
  email = ActionMailer::Base.deliveries.last
  m = email.body.match /.*"([^"]*)".*$/
  visit m[1]
end

Then /^My user account should be confirmed$/ do
  user = User.find_by_email(@visitor.email)
  user.confirmed?.should == true
end

Given /^(?:I am )?a registered(, unconfirmed)? user$/ do |unconfirmed|
  @user = create(:user)

  if not unconfirmed
    @user.confirm!
  end
end

Given /^a confirmed user "([^"]*)"$/ do |email|
  @user = create(:user, :email => email)
  @user.confirm!
end

Given /^I am signed in$/ do
  step "a registered user"
  step "I sign in"
  
end

Given /^I am signed out$/ do
  visit destroy_user_session_path
end

When /^I am at the sign in page$/ do
  visit new_user_session_path
end

When /^want to recover my password$/ do
  click_link "Forgot your password?"
end

When /^enter my email adress$/ do
  fill_in "user_email", :with => @user.email
  # delete old emails first
  ActionMailer::Base.deliveries = []
  click_button "Send me reset password instructions"
end

Then /^an email with possword recovery instructions should have been sent to my adress\.$/ do
  email = ActionMailer::Base.deliveries.last
  email.from.should == ["noreply@example.com"]
  email.to.should == [@user.email]
  email.body.should include "change your password"
end

Given /^I requested password recovery instructions$/ do
  visit destroy_user_session_path
  visit new_user_session_path
  click_link "Forgot your password?"
  fill_in "user_email", :with => @user.email
  # delete old emails first
  ActionMailer::Base.deliveries = []
  click_button "Send me reset password instructions"
end

When /^enter a new password$/ do
  @new_password = "123456789"
  fill_in "user_password", :with => @new_password
  fill_in "user_password_confirmation", :with => @new_password
  click_button "Change my password"
end

Then /^I should be signed in$/ do
  page.should have_content @user.email
end

Then /^I should be able to sign in with the new password$/ do
  visit destroy_user_session_path
  visit new_user_session_path
  fill_in "user_email", :with => @user.email
  fill_in "user_password", :with => @new_password
  click_button "Sign in"
  
  visit '/'
  page.should have_content @user.email
end

When /^I sign in$/ do
  visit new_user_session_path
  fill_in "user_email", :with => @user.email
  fill_in "user_password", :with => @user.password
  click_button "Sign in"
end



When /^I try to sign in with "([^"]*)" and "([^"]*)"$/ do |email, password|
  visit new_user_session_path
  fill_in "user_email", :with => email
  fill_in "user_password", :with => password
  click_button "Sign in"
end

Then /^I should see the following message "([^"]*)"$/ do |msg|
  page.should have_content msg
end

When /^I try to register with "([^"]*)" and "([^"]*)" and "([^"]*)"$/ do |email, pwd, confirmation|
  visit new_user_registration_path
  fill_in "user_email", :with => email
  fill_in "user_password", :with => pwd
  fill_in "user_password_confirmation", :with => confirmation
  click_button "Sign up"
end

When /^I try to register a new account with the same email address$/ do
  visit new_user_registration_path
  fill_in "user_email", :with => @user.email
  fill_in "user_password", :with => "valid_pw"
  fill_in "user_password_confirmation", :with => "valid_pw"
  click_button "Sign up"
end

Given /^I sign in with(out)? remember\-me$/ do |dont_remember|
  visit new_user_session_path
  fill_in "user_email", :with => @user.email
  fill_in "user_password", :with => @user.password  
  
  if dont_remember
    uncheck "user_remember_me"
  else
    check "user_remember_me"
  end
  
  click_button "Sign in"
end

Given /^I close the browser window$/ do
  expire_cookies
end

When /^I come back to the site$/ do
  visit '/'
end

Given /^I am on the confirmation page$/ do
  visit new_user_confirmation_path
end

When /^I request new confirmation instructions$/ do
  fill_in "user_email", :with => @user.email
  click_button "Resend confirmation instructions" 
end

When /^I visit "([^"]*)"$/ do |path|
  visit path
end

Then /^I should be on the hub page$/ do
  current_path = URI.parse(current_url).path
  current_path.should == hub_path
end

Given /^I am a visitor$/ do
  # don't do anything. Unless we don't do anything before it, each test
  # session is freshly initialized
end

When /^I visit the hub page$/ do
  visit hub_path
end

Then /^I should be on the sign in page$/ do
  current_path = URI.parse(current_url).path
  current_path.should == new_user_session_path
end

Then /^I should see "([^"]*)"$/ do |content|
  page.should have_content content
end

When /^I am on the hub page$/ do
  step "I visit the hub page"
end

Then /^then I should see a link to the account settings$/ do
  link = find_link "account settings"
  link.should_not be_nil
end

Given /^I am on the account settings page$/ do
  visit edit_user_registration_path
end

When /^I change my password$/ do
  @new_password = "123456789"
  fill_in "user_email", :with => @user.email
  fill_in "user_password", :with => @new_password
  fill_in "user_password_confirmation", :with => @new_password
  fill_in "user_current_password", :with => @user.password
  click_button "Update"
end

Given /^I am signed in as "([^"]*)" with password "([^"]*)"$/ do |email, password|
  @user = create(:user, :email => email, :password => password)
  @user.confirm!
  step "I sign in"
end

When /^I change my password with "([^"]*)" "([^"]*)" "([^"]*)"$/ do |pwd, conf, current_pwd|
  fill_in "user_email", :with => @user.email
  fill_in "user_password", :with => pwd
  fill_in "user_password_confirmation", :with => conf
  fill_in "user_current_password", :with => current_pwd
  click_button "Update"
end

When /^I change my email address$/ do
  @new_email = "new@example.com"
  fill_in "user_email", :with => @new_email
  fill_in "user_current_password", :with => @user.password
  click_button "Update"
end

Then /^I should be able to sign in with the new email address$/ do
  visit destroy_user_session_path
  step "I try to sign in with \"#{@new_email}\" and \"#{@user.password}\""
  visit hub_path
  page.should have_content @new_email
end

Then /^a confirmation email should have been sent to the new email address$/ do
  email = ActionMailer::Base.deliveries.last
  email.from.should == ["noreply@example.com"]
  email.to.should == [@new_email]
  email.body.should include "confirm your account"
end
