Feature: Show developer page
	In order to inform myself about a developer
	As a Visitor
	I want to see the developer's page
	
	Scenario: Show developer's page
		Given a developer called "Bender"
		When I am on the developer's page
		Then I should see the developer's name
