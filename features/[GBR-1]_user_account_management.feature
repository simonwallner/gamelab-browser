Feature: [GBR-1] User Account Management
	As a user,
	as a prerequisite to manage content,
	I want to create and manage a user account
	
	Scenario: Register a new user account
		Given I am an unregistered visitor 
		And I am on the sign up page
		When I enter my registration credentials
		Then a user account should have been created for me
		And a confirmation email should have been sent to me
		And I should not be signed in
	
	Scenario: Confirm new user account
		Given I am an unregistered visitor
		And I register a user account
		And I follow the confirmation link in the email
		Then My user account should be confirmed
	
	Scenario: Resend confirmation instructions
		Given I am a registered, unconfirmed user
		And I am on the confirmation page
		When I request new confirmation instructions
		Then a confirmation email should have been sent to me
	
	Scenario Outline: Fail to register a new user account with invalid credentials
		Given I am on the sign up page
		When I try to register with <user> and <pwd> and <conf>
		Then I should see the following message <message>
		
		Examples:
		 | user                   | pwd        | conf         | message                               |
		 | "valid@example.com"    | "short"    | "short"      | "Password is too short"               |
		 | "valid@example.com"    | "valid_pw" | "wrong_conf" | "Password doesn't match confirmation" |
		 | "invalid.email.adress" | "valid_pw" | "valid_pw"   | "Email is invalid"                    |
	
	Scenario: Fail to register a new user account due to existing account
		Given a registered user
		When I try to register a new account with the same email address
		Then I should see the following message "Email has already been taken"
		
	Scenario: Sign in
		Given I am a registered user
		When I sign in
		Then I should be signed in
	
	Scenario Outline: Fail to sign in with confirmed user
		Given a confirmed user "bender@example.com"
		When I try to sign in with <user> and <password>
		Then I should see the following message <message>
		
		Examples:
		 | user                 | password         | message                     |
		 | "wrong@example.com"  | "foobar"         | "Invalid email or password" |
		 | "bender@example.com" | "wrong_password" | "Invalid email or password" |

	
	Scenario: Get password recovery instructions
		Given I am a registered user
		And I am signed out
		When I am at the sign in page
		And want to recover my password
		And enter my email adress
		Then an email with possword recovery instructions should have been sent to my adress.
	
	Scenario: Recover password with recovery link
		Given I am a registered user
		And I requested password recovery instructions
		When I follow the recovery link in the email
		And enter a new password
		Then I should be signed in
		And I should be able to sign in with the new password
	
	Scenario: Thou shalt not remember me
		Given I am a registered user
		And I sign in without remember-me
		And I close the browser window
		When I come back to the site
		Then I should not be signed in
	
	Scenario: Thou shalt remember me
		Given I am a registered user
		And I sign in with remember-me
		And I close the browser window
		When I come back to the site
		Then I should be signed in
	
	Scenario: Change password on account settings page
		Given I am signed in
		And I am on the account settings page
		When I change my password
		Then I should be able to sign in with the new password
	
	Scenario Outline: Fail to change password
		Given I am signed in as "bender@example.com" with password "123456"
		And I am on the account settings page
		When I change my password with <pwd> <conf> <current_pwd>
		Then I should see the following message <msg>

		Examples:
		 | pwd       | conf         | current_pwd | msg                                   |
		 | "foobar"  | "wrong_conf" | "123456"    | "Password doesn't match confirmation" |
		 | "foobar"  | "foobar"     | ""          | "Current password can't be blank"     |
		 | "ignored" | "ignored"    | "wrong_pw"  | "Current password is invalid"         |

	Scenario: Change and confirm new email address
		Given I am signed in
		And I am on the account settings page
		When I change my email address
		Then a confirmation email should have been sent to the new email address
		And I should be signed in
		When I follow the confirmation link in the email
		Then I should be able to sign in with the new email address
	
	Scenario: Wrong email confirmation token
		When I visit "/users/confirmation?confirmation_token=wrongTokenisWrong"
		Then I should see "Confirmation token is invalid"










