require 'spec_helper'

describe HubController do

  describe "GET 'view'" do
    it "redirects to the sign up page if the visitor is not signed in" do
      get 'view'
      response.should redirect_to(new_user_session_path)
    end
  end
  
  describe "GET 'view'" do
    it "returns http success if the user is signed in" do
      user = Factory(:user)
      user.confirm!
      
      sign_in :user, user
      get 'view'
      response.should be_success
    end
  end

end
